package Max.cofee

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private var scope:Int = 0
    private var price:Int = 0

    @SuppressLint("SetTextI18n")
    fun buttonDownFun(view: View) {
        if(scope>0) scope--
        quantity_text_view.text = scope.toString()


    }

    @SuppressLint("SetTextI18n")
    fun button_up_fun(view: View) {
        scope++
        quantity_text_view.text = scope.toString()
    }

    @SuppressLint("SetTextI18n")
    fun submitOrder(view: View) {
        var milk = 0
        if (checkBoxMilk.isChecked) milk+=2
        price = (4 + milk) * scope

        val order:String = editText.text.toString()


        price_num_view.text = "$$price \n \n $order"

    }

    fun startMain2(view: View){
        val mainA = Intent(this, Main2Activity::class.java)
        setContentView(R.layout.activity_main2)
        startActivity(mainA)
    }
}
