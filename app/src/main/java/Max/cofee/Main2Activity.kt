package Max.cofee

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main2.*
import kotlin.random.Random

class Main2Activity : AppCompatActivity() {
    var num:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
    }

    fun toast(view: View){
        val toast = Toast.makeText(this, "Hi Max", Toast.LENGTH_LONG)
        toast.show()
    }


    fun count(view: View) {

        num++
        textView.text = num.toString()
    }


    fun rand(view: View){
        textView.text = Random.nextInt(15).toString()
    }



}
